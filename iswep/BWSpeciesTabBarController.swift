//
//  BWSpeciesTabBarController.swift
//  iswep
//
//  Created by Jordan Daniel on 10/15/18.
//  Copyright © 2018 UGA Center for Invasive Species and Ecosystem Health. All rights reserved.
//

import UIKit

class BWSpeciesTabBarController: UITabBarController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    var speciesImages = [[String:String]]()
    var currentImageIndex : Int = 0
    var forTransitionIndex : Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissSpeciesDetails(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if currentImageIndex > 0 {
            let sb = UIStoryboard.init(name: "Main", bundle: Bundle.main)
            let beforeIndex : Int = currentImageIndex-1
            let beforeController = sb.instantiateViewController(withIdentifier: "BWPhotoViewerUI") as! BWPhotoViewerController
            beforeController.imageIndex = beforeIndex
            beforeController.speciesImageName = speciesImages[beforeIndex]["filename"]
            beforeController.imageCaption = speciesImages[beforeIndex]["credit"]
            
            return beforeController
            
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if currentImageIndex < (speciesImages.count - 1) {
            let sb = UIStoryboard.init(name: "Main", bundle: Bundle.main)
            let afterIndex : Int = currentImageIndex+1
            let afterController = sb.instantiateViewController(withIdentifier: "BWPhotoViewerUI") as! BWPhotoViewerController
            afterController.imageIndex = afterIndex
            afterController.speciesImageName = speciesImages[afterIndex]["filename"]
            afterController.imageCaption = speciesImages[afterIndex]["credit"]
            
            return afterController
        }
       
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return speciesImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentImageIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        let nextController = pendingViewControllers.first as! BWPhotoViewerController
        forTransitionIndex = nextController.imageIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let lastController = previousViewControllers.first as! BWPhotoViewerController
        let displayedControllerIndex = lastController.imageIndex
        if finished && completed {
            if displayedControllerIndex < forTransitionIndex!{
                currentImageIndex += 1
            }else if displayedControllerIndex > forTransitionIndex!{
                currentImageIndex -= 1
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
