//
//  FSTableViewController.swift
//  iswep
//
//  Created by Jordan Daniel on 10/1/18.
//  Copyright © 2018 UGA Center for Invasive Species and Ecosystem Health. All rights reserved.
//

import UIKit
import SQLite

class FSTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AboutViewSegue" {
            if let path = Bundle.main.path(forResource: "about", ofType: "html"){
                do{
                    let quickViewController = (segue.destination as! UINavigationController).topViewController as! BWQuickViewController
                    quickViewController.popoverPresentationController?.sourceView = sender as? UIView
                    quickViewController.titleString = "About"
                    quickViewController.htmlString = try String.init(contentsOfFile: path)
                }catch{
                    NSLog("Error getting Quick View")
                }
            }
        }else if segue.identifier == "PlantListsSegue" {
            let dbPath = Bundle.main.path(forResource: "test_category", ofType: "sqlite")
            do{
                let dbData = try Connection(dbPath!, readonly:true)
                let speciesTable = Table("weeds_species_data")
                let subid = Expression<Int64>("id")
                let comname = Expression<String?>("common_name")
                let sciname = Expression<String?>("scientific_name")
                let family = Expression<String?>("family")
                let nativela = Expression<String?>("native_look_alike")
                let id_descript = Expression<String?>("id_description")
                let ecoimpact = Expression<String?>("ecological_impact")
                let distroabund = Expression<String?>("distri_and_abund")
                let management = Expression<String?>("management")
                let native = Expression<String?>("native")
                let images = Expression<String?>("images")
                let danger = Expression<String?>("danger")
                let veg = Expression<String?>("vegetation")
                let vegfall = Expression<String?>("vegetationfall")
                let stem = Expression<String?>("stem")
                let bloom = Expression<String?>("bloom")
                let plantheight = Expression<String?>("plantheight")
                let leafheight = Expression<String?>("leafheight")
                let leaf = Expression<String?>("leaf")
                let root = Expression<String?>("root")
                let planttraits = Expression<String?>("planttraits")
                let query = speciesTable.select([subid,comname,sciname,family,nativela,id_descript,ecoimpact,distroabund,management,native,danger,images,veg,vegfall,stem,bloom,plantheight,leafheight,leaf,root,planttraits])
                                        .order(comname.asc)
                //                let speciesLists = try dbData.prepare(query).map {row in return try row.decode()}
                var speciesLists : Array<Array<Dictionary<String,Any>>> = [Array(),Array()]
                for species in try dbData.prepare(query) {
                    
                    let speciesDic : Dictionary<String,Any> = ["id":species[subid],
                                                               "comname":species[comname] ?? "",
                                                               "sciname":species[sciname] ?? "",
                                                               "family":species[family] ?? "",
                                                               "nativela":species[nativela] ?? "",
                                                               "id_descript":species[id_descript] ?? "",
                                                               "ecoimpact":species[ecoimpact] ?? "",
                                                               "distroabund":species[distroabund] ?? "",
                                                               "management":species[management] ?? "",
                                                               "native":species[native] ?? "",
                                                               "images":species[images] ?? "",
                                                               "danger":species[danger] ?? "",
                                                               "veg":species[veg] ?? "",
                                                               "vegfall":species[vegfall] ?? "",
                                                               "stem":species[stem] ?? "",
                                                               "bloom":species[bloom] ?? "",
                                                               "plantheight":species[plantheight] ?? "",
                                                               "leafheight":species[leafheight] ?? "",
                                                               "leaf":species[leaf] ?? "",
                                                               "root":species[root] ?? "",
                                                               "planttraits":species[planttraits] ?? ""]
                    if species[native] == "0" {
                        speciesLists[1].append(speciesDic)
                    }else{
                        speciesLists[0].append(speciesDic)
                    }
                    let plantsTabController : UITabBarController = segue.destination as! UITabBarController
                    let pullTable = plantsTabController.viewControllers![0] as! SpeciesTableController
                    pullTable.species = Array([speciesLists[1]])
                    let keepTable = plantsTabController.viewControllers![1] as! SpeciesTableController
                    keepTable.species = Array([speciesLists[0]])
                    
                }
                
            }catch{
                print(error)
            }
        }else if segue.identifier == "PlantListsSegue"{
            
        }
    }
    

}
