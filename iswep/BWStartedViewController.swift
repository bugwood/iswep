//
//  BWStartedViewController.swift
//  iswep
//
//  Created by Jordan Daniel on 9/28/18.
//  Copyright © 2018 UGA Center for Invasive Species and Ecosystem Health. All rights reserved.
//

import UIKit

class BWStartedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func viewQuickDataPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "QuickViewSegue", sender: sender)
    }
    
    @IBAction func getStartedPressed(_ sender: Any) {
        performSegue(withIdentifier: "GetStartedSegue", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "QuickViewSegue" {            
            if let path = Bundle.main.path(forResource: "misc_info", ofType: "json"){
                do{
                    let quickViews:Array = try JSONSerialization.jsonObject(with:NSData(contentsOfFile: path)! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! Array<Dictionary<String, String>>
                    let quickViewData:Dictionary<String,String> = quickViews[(sender as! UIButton).tag]
                    
                    let quickViewController = (segue.destination as! UINavigationController).topViewController as! BWQuickViewController
                    
                    quickViewController.popoverPresentationController?.sourceView = sender as? UIView
                    quickViewController.titleString = quickViewData["title"]
                    quickViewController.htmlString = quickViewData["html"]
                }catch{
                    NSLog("Error getting Quick View")
                }
            }
            
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
