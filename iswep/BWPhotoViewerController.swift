//
//  BWPhotoViewerController.swift
//  iswep
//
//  Created by Jordan Daniel on 10/15/18.
//  Copyright © 2018 UGA Center for Invasive Species and Ecosystem Health. All rights reserved.
//

import UIKit

class BWPhotoViewerController: UIViewController,UIScrollViewDelegate {
    var imageIndex : Int = 0
    var speciesImageName : String?
    var imageCaption : String?
    @IBOutlet weak var speciesImageIV: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        speciesImageIV.image = UIImage(named: speciesImageName!)
        captionLabel.text = imageCaption!
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return speciesImageIV
    }
}
