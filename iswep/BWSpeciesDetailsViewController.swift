//
//  BWSpeciesDetailsViewController.swift
//  iswep
//
//  Created by Jordan Daniel on 10/17/18.
//  Copyright © 2018 UGA Center for Invasive Species and Ecosystem Health. All rights reserved.
//

import UIKit

class BWSpeciesDetailsViewController: UIViewController {
    var speciesDetails = [String:Any]()
    
    @IBOutlet var sciNameLabel: UILabel!
    @IBOutlet var comNameLabel: UILabel!
    @IBOutlet var nativelaTV: UITextView!
    @IBOutlet var descriptTV: UITextView!
    @IBOutlet var ecoTV: UITextView!
    @IBOutlet var distroTV: UITextView!
    @IBOutlet var managementTV: UITextView!
    @IBOutlet var springVegTV: UITextView!
    @IBOutlet var fallVegTV: UITextView!
    @IBOutlet var stemTV: UITextView!
    @IBOutlet var bloomTV: UITextView!
    @IBOutlet var heightTV: UITextView!
    @IBOutlet var leafDescriptTV: UITextView!
    @IBOutlet var leafHeightTV: UITextView!
    @IBOutlet var plantTraitTV: UITextView!
    @IBOutlet var rootDescriptTV: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        comNameLabel.text = speciesDetails["comname"] as? String
        sciNameLabel.text = speciesDetails["sciname"] as? String
        nativelaTV.text = speciesDetails["nativela"] as? String
        descriptTV.text = speciesDetails["id_descript"] as? String
        ecoTV.text = speciesDetails["ecoimpact"] as? String
        distroTV.text = speciesDetails["distroabund"] as? String
        managementTV.text = speciesDetails["management"] as? String
        springVegTV.text = speciesDetails["veg"] as? String
        fallVegTV.text = speciesDetails["vegfall"] as? String
        stemTV.text = speciesDetails["stem"] as? String
        bloomTV.text = speciesDetails["bloom"] as? String
        heightTV.text = speciesDetails["plantheight"] as? String
        leafDescriptTV.text = speciesDetails["leaf"] as? String
        leafHeightTV.text = speciesDetails["leafheight"] as? String
        plantTraitTV.text = speciesDetails["planttraits"] as? String
        rootDescriptTV.text = speciesDetails["root"] as? String
        
        nativelaTV.sizeToFit()
        descriptTV.sizeToFit()
        ecoTV.sizeToFit()
        distroTV.sizeToFit()
        managementTV.sizeToFit()
        springVegTV.sizeToFit()
        fallVegTV.sizeToFit()
        stemTV.sizeToFit()
        bloomTV.sizeToFit()
        heightTV.sizeToFit()
        leafDescriptTV.sizeToFit()
        leafHeightTV.sizeToFit()
        plantTraitTV.sizeToFit()
        rootDescriptTV.sizeToFit()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
