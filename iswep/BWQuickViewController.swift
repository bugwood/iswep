//
//  BWQuickViewController.swift
//  iswep
//
//  Created by Jordan Daniel on 9/28/18.
//  Copyright © 2018 UGA Center for Invasive Species and Ecosystem Health. All rights reserved.
//

import UIKit
import WebKit

class BWQuickViewController: UIViewController {
    var titleString : String?
    var htmlString : String?
    @IBOutlet var htmlWV: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = (titleString ?? "")
        htmlWV.loadHTMLString((htmlString ?? ""), baseURL: nil)
    }
    
    @IBAction func doneViewingQuickDataPressed(_ sender: Any) {
        self.dismiss(animated:true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
