//
//  KeyStepViewController.swift
//  iswep
//
//  Created by Jordan Daniel on 10/4/18.
//  Copyright © 2018 UGA Center for Invasive Species and Ecosystem Health. All rights reserved.
//

import UIKit
import SQLite

class KeyStepViewController: UITableViewController {
    var selectionStack = [Int]()
    @IBOutlet var cell1: UITableViewCell!
    @IBOutlet var cell2: UITableViewCell!
    @IBOutlet var cell3: UITableViewCell!
    @IBOutlet var cell4: UITableViewCell!
    @IBOutlet var cell5: UITableViewCell!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        if self.selectionStack.count > 0 {
            let startOver = UIBarButtonItem(title: "Start Over",
                                          style: UIBarButtonItem.Style.plain,
                                          target: self,
                                          action: #selector(startOverButtonPressed(sender:)))
            self.navigationItem.setRightBarButton(startOver, animated: true)
        }
    }
    
    @objc func startOverButtonPressed(sender :UIBarButtonItem){
        self.performSegue(withIdentifier: "StartOverUnwind", sender: nil)
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StartOverUnwind" ||
            segue.identifier == "BackUnwind"{return}
        print("caused a segue\n-------------------- \n",sender ?? "its blank")
        print("before selectionStack - \(self.selectionStack)")
        let selectedCell = sender as! UITableViewCell
//        selectedCell.accessoryType = UITableViewCell.AccessoryType.checkmark
        let selectedIndexPath = self.tableView.indexPath(for: selectedCell)
        let newSelection : Int = ((selectedIndexPath?.section)! > 0) ? ((selectedIndexPath?.row)!+3):((selectedIndexPath?.row)!+1)
        var newSelectionStack = Array(self.selectionStack)
        newSelectionStack.append(newSelection)
        
        if segue.identifier == "FilteredSpeciesSegue" {
            let dbPath = Bundle.main.path(forResource: "test_category", ofType: "sqlite")
            do{
                let dbData = try Connection(dbPath!, readonly:true)
                let speciesTable = Table("weeds_species_data")
                let subid = Expression<Int64>("id")
                let comname = Expression<String?>("common_name")
                let sciname = Expression<String?>("scientific_name")
                let family = Expression<String?>("family")
                let nativela = Expression<String?>("native_look_alike")
                let id_descript = Expression<String?>("id_description")
                let ecoimpact = Expression<String?>("ecological_impact")
                let distroabund = Expression<String?>("distri_and_abund")
                let management = Expression<String?>("management")
                let native = Expression<String?>("native")
                let images = Expression<String?>("images")
                let danger = Expression<String?>("danger")
                let veg = Expression<String?>("vegetation")
                let vegfall = Expression<String?>("vegetationfall")
                let stem = Expression<String?>("stem")
                let bloom = Expression<String?>("bloom")
                let plantheight = Expression<String?>("plantheight")
                let leafheight = Expression<String?>("leafheight")
                let leaf = Expression<String?>("leaf")
                let root = Expression<String?>("root")
                let planttraits = Expression<String?>("planttraits")
                let category = Expression<String?>("category")
                let categoryextra = Expression<String?>("categoryextra")
//                print("selectionStack - \(self.selectionStack)")
                let selectionStringArray = newSelectionStack.map({String($0)})
                let selectionString = selectionStringArray.joined(separator:"")
                print("selectionString - \(selectionString)")
                let query = speciesTable.select([subid,comname,sciname,family,nativela,id_descript,ecoimpact,distroabund,management,native,danger,images,veg,vegfall,stem,bloom,plantheight,leafheight,leaf,root,planttraits])
                                        .filter(category == selectionString || categoryextra == selectionString)
                                        .order(comname.asc)
//                let speciesLists = try dbData.prepare(query).map {row in return try row.decode()}
                var speciesLists : Array<Array<Dictionary<String,Any>>> = [Array(),Array()]
                for species in try dbData.prepare(query) {
                    
                    let speciesDic : Dictionary<String,Any> = ["id":species[subid],
                                                     "comname":species[comname] ?? "",
                                                     "sciname":species[sciname] ?? "",
                                                     "family":species[family] ?? "",
                                                     "nativela":species[nativela] ?? "",
                                                     "id_descript":species[id_descript] ?? "",
                                                     "ecoimpact":species[ecoimpact] ?? "",
                                                     "distroabund":species[distroabund] ?? "",
                                                     "management":species[management] ?? "",
                                                     "native":species[native] ?? "",
                                                     "images":species[images] ?? "",
                                                     "danger":species[danger] ?? "",
                                                     "veg":species[veg] ?? "",
                                                     "vegfall":species[vegfall] ?? "",
                                                     "stem":species[stem] ?? "",
                                                     "bloom":species[bloom] ?? "",
                                                     "plantheight":species[plantheight] ?? "",
                                                     "leafheight":species[leafheight] ?? "",
                                                     "leaf":species[leaf] ?? "",
                                                     "root":species[root] ?? "",
                                                     "planttraits":species[planttraits] ?? ""]
                    if species[native] == "1" {
                        speciesLists[1].append(speciesDic)
                    }else{
                        speciesLists[0].append(speciesDic)
                    }
                    let speciesTV = segue.destination as! SpeciesTableController
                    speciesTV.species = speciesLists
                    
                }
                
            }catch{
                print(error)
            }
        }else{
            let nextSelection : KeyStepViewController = segue.destination as! KeyStepViewController
            nextSelection.selectionStack = newSelectionStack
        }
        
    }

}
